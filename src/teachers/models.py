import random

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker


class Teacher(models.Model):
    first_name = models.CharField(
        max_length=120, blank=True, null=True, validators=[MinLengthValidator(3)]
    )
    last_name = models.CharField(
        max_length=120, blank=True, null=True, validators=[MinLengthValidator(3)]
    )
    email = models.EmailField(max_length=120)
    grade = models.PositiveSmallIntegerField(null=True, default=0)
    item = models.CharField(max_length=120, null=True)
    start_worked_in_course = models.DateField(null=True)

    def __str__(self):
        return f"{self.first_name}_{self.last_name} {self.email}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                grade=random.choice(range(1, 100)),
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                start_worked_in_course=faker.date_time(),
            )
