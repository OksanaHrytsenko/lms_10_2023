from django.db.models import Q
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from marshmallow import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher
from teachers.utils.helpers import format_records


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello teachers")


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    },
    location="query",
)
def get_teachers(request: HttpRequest, params) -> HttpResponse:
    teachers = Teacher.objects.all()

    search_fields = ["last_name", "first_name", "email"]

    for name, value in params.items():
        if name == "search_text":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": value})
            teachers = teachers.filter(or_filter)

        else:
            teachers = teachers.filter(**{name: value})

    result = format_records(teachers)
    return HttpResponse(result)


@csrf_exempt
def create_teacher(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TeacherForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))

    elif request.method == "GET":
        form = TeacherForm()
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Create</button>
    </form>
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_teacher(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=teacher)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        form = TeacherForm(instance=teacher)
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Create</button>
    </form>
    """
    return HttpResponse(form_html)


@csrf_exempt
def delete_teacher(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=teacher)

        if form.is_valid():
            form.delete()

            return HttpResponse(f"teacher delete: {teacher}", status=404)
    elif request.method == "GET":
        form = TeacherForm(instance=teacher)
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Delete</button>
    </form>
    """
    return HttpResponse(form_html)
