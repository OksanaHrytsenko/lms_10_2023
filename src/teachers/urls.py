from django.urls import path

from teachers.views import (
    index,
    create_teacher,
    get_teachers,
    update_teacher,
    delete_teacher,
)

app_name = "teachers"

urlpatterns = [
    path("index/", index, name="index"),
    path("", get_teachers, name="get_teachers"),
    path("create/", create_teacher, name="create_teacher"),
    path("update/<int:pk>/", update_teacher, name="update_teacher"),
    path("delete/<int:pk>/", delete_teacher, name="delete_teacher"),
]
