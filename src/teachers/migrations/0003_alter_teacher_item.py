# Generated by Django 4.2.5 on 2023-10-31 00:35

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("teachers", "0002_alter_teacher_item_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="teacher",
            name="item",
            field=models.CharField(max_length=120, null=True),
        ),
    ]
