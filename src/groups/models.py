import random
from django.db import models
from faker import Faker


class Group(models.Model):
    group_number = models.PositiveSmallIntegerField(null=True, default=1)
    teacher_name = models.CharField(max_length=120, blank=True, null=True)
    grade = models.PositiveSmallIntegerField(null=True, default=0)
    start_date_course = models.DateField(null=True)
    end_date_course = models.DateField(null=True)
    number_of_students = models.PositiveSmallIntegerField(max_length=18, null=True)

    def __str__(self):
        return f"{self.group_number}{self.teacher_name}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                group_number=random.choice(range(1, 30)),
                teacher_name=faker.last_name(),
                grade=random.choice(range(1, 12)),
                start_date_course=faker.date_time(),
                end_date_course=faker.date_time(),
                number_of_students=random.choice(range(1, 18)),
            )
