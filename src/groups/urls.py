from django.urls import path

from groups.views import index, get_groups, create_group, update_group, delete_group

app_name = "groups"

urlpatterns = [
    path("index", index, name="index"),
    path("", get_groups, name="get_groups"),
    path("create/", create_group, name="create_group"),
    path("update/<int:pk>/", update_group, name="update_group"),
    path("delete/<int:pk>/", delete_group, name="delete_group"),
]
