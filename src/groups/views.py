from django.db.models import Q
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from groups.forms import GroupForm
from groups.models import Group
from groups.utils.helpers import format_records


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello group")


@use_args(
    {
        "group_number": fields.Str(required=False),
        "teacher_name": fields.Str(required=False),
        "start_date_course": fields.Date(required=False),
        "search_text": fields.Str(required=False),
    },
    location="query",
)
def get_groups(request: HttpRequest, params) -> HttpResponse:
    groups = Group.objects.all()

    search_fields = ["group_number", "teacher_name", "start_date_course"]

    for name, value in params.items():
        if name == "search_text":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": value})
                groups = groups.filter(or_filter)

        else:
            groups = groups.filter(**{name: value})

    result = format_records(groups)
    return HttpResponse(result)


@csrf_exempt
def create_group(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = GroupForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_groups"))
    elif request.method == "GET":
        form = GroupForm()
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Create</button>
    </form>
    """
    return HttpResponse(form_html)


@csrf_exempt
def update_group(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_groups"))
    elif request.method == "GET":
        form = GroupForm(instance=group)
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Create</button>
    </form>
    """
    return HttpResponse(form_html)


@csrf_exempt
def delete_group(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)

        if form.is_valid():
            form.delete()
            return HttpResponse(f"group delete: {group}", status=404)
    elif request.method == "GET":
        form = GroupForm(instance=group)
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <button type="submit" value="Submit">Delete</button>
    </form>
    """
    return HttpResponse(form_html)
