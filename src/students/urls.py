from django.urls import path

from students.views import (
    index,
    get_students,
    create_student,
    update_student,
    delete_student,
    students_error_404,
)

app_name = "students"

urlpatterns = [
    path("index/", index, name="index"),
    path("", get_students, name="get_students"),
    path("create/", create_student, name="create_student"),
    path("update/<int:pk>/", update_student, name="update_student"),
    path("delete/<int:pk>/", delete_student, name="delete_student"),
    path("__debug__/", students_error_404, name="students_error_404"),
]
