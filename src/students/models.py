import datetime
import random

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from students.utils.validators import first_name_validator


class Student(models.Model):
    first_name = models.CharField(
        max_length=120,
        blank=True,
        null=True,
        validators=[MinLengthValidator(3), first_name_validator],
    )
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=120)
    grade = models.PositiveSmallIntegerField(null=True, default=0)
    birth_date = models.DateField(null=True)

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name}_{self.last_name} {self.email} ({self.id})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                grade=random.choice(range(1, 12)),
                birth_date=faker.date_time_between(start_date="-40y", end_date="-18y"),
            )
