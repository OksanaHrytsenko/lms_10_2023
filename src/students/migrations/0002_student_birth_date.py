# Generated by Django 4.2.5 on 2023-10-28 19:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("students", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="student",
            name="birth_date",
            field=models.DateField(null=True),
        ),
    ]
