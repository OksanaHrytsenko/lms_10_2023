from django.db.models import Q
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student


def index(request: HttpRequest) -> HttpResponse:
    return render(request, template_name="index.html", context={"key": "Oksana"})


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    },
    location="query",
)
def get_students(request: HttpRequest, params) -> HttpResponse:
    students = Student.objects.all()

    search_fields = ["last_name", "first_name", "email"]

    for name, value in params.items():
        if name == "search_text":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": value})
            students = students.filter(or_filter)

        else:
            students = students.filter(**{name: value})

    # result = format_records(students)
    return render(
        request, template_name="students/list.html", context={"students": students}
    )


@csrf_exempt
def create_student(request: HttpRequest) -> HttpResponse:
    # form = """<form method="post">
    # <label for="FirstNameId">First name:</label><br>
    # <input type="text" id="FirstNameId" name="first_name" placeholder="Mikhailo" value="Student"><br>

    # <label for="LastNameId">Last name:</label><br>
    # <input type="text" id="LastNameId" name="last_name" placeholder="Lazoryk" value="Student"><br>

    # <label for="EmailId">Email:</label><br>
    #  <input type="email" id="EmailId" name="email" placeholder="admin@admin.com" value="admin@admi.com"><br>

    # <label for="GradeId">Grade:</label><br>
    # <input type="number" id="GradeId" name="grade" placeholder="100" value=100><br>

    # <button type="submit" value="Submit">Create</button>
    # </form>
    # """

    if request.method == "POST":
        form = StudentForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))

    elif request.method == "GET":
        form = StudentForm()

    return render(request, template_name="students/create.html", context={"form": form})


@csrf_exempt
def update_student(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request, template_name="students/update.html", context={"form": form})


@csrf_exempt
def delete_student(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.delete()
            return HttpResponse(f"student delete: {student}", status=404)
    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request, template_name="students/delete.html", context={"form": form})


def students_error_404(request, exception):
    return render(request, template_name="system_errors/404_errors.html", status=404)
